import React from 'react'
import {Text, StyleSheet, View, Button} from 'react-native';

const Nosotros = ({navigation, route}) => {

  const {cliendId, total} = route.params;

  const visitarNosotros = () => {
    // navigation.navigate('Inicio');
    navigation.goBack();

  }

  return (
    <View style={styles.container}>
      <Text>Nosotros</Text>
      <Button
        title='ir a Inicio'
        onPress={() => visitarNosotros()}
      />
      <Text>Cliente: {cliendId}</Text>
      <Text>Total: {total}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default Nosotros;