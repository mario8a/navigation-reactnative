import React from 'react'
import {Text, StyleSheet, View, Button} from 'react-native';

const Inicio = ({navigation}) => {

  const informacion = {
    cliendId: 'Mario',
    total: 500
  }

  const visitarNosotros = () => {
    navigation.navigate('Nosotros', informacion);
  }

  return (
    <View style={styles.container}>
      <Text>Inicio</Text>
      <Button
        title='ir a nosotros'
        onPress={() => visitarNosotros()}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default Inicio;