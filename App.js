import 'react-native-gesture-handler';
import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Inicio from './screens/Inicio';
import Nosotros from './screens/Nosotros';
const Stack = createStackNavigator();

const App = () => {

  return (
    <NavigationContainer>
      <Stack.Navigator 
        initialRouteName='Inicio'
        screenOptions={{
          title: 'Componente principal',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#F4511E'
          },
          headerTintColor: '#FFF',
          headerTitleStyle: {
            fontWeight: 'bold'
          }
        }}
      >
        <Stack.Screen 
          name="Inicio" 
          component={Inicio} 
          // options={{
          //   title: 'Componente principal',
          //   headerTitleAlign: 'center',
          //   headerStyle: {
          //     backgroundColor: '#F4511E'
          //   },
          //   headerTintColor: '#FFF',
          //   headerTitleStyle: {
          //     fontWeight: 'bold'
          //   }
          // }}
        />
        <Stack.Screen name="Nosotros" component={Nosotros} options={({route}) => ({
          title: route.params.cliendId
        })} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({

});

export default App;
